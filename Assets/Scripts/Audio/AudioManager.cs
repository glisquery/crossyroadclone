﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager instance = null;
    private static readonly object padlock = new object();
    
    [SerializeField]
    private AudioPlayer audioPlayerPrefab; 
    private delegate void AudioPlayerInvoker(string path);

    public static AudioManager Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    }

    public void UpdateState()
    {
        var mute = GameConfig.Actual.mute;
        if (mute)
        {
            AudioPlayer.volume = 0f;
        }
        else
        {
            AudioPlayer.volume = 1f;
        }
        var players = FindObjectsOfType<AudioPlayer>();
        foreach (var player in players)
        {
            player.UpdateVolume();
        }
    }
    
    public void Play(string path)
    {
        GameObject audioPlayer = Pool.Get("AudioPlayer");
        audioPlayer.GetComponent<AudioPlayer>().Play(path);
    }

    private void Awake()
    {
        GameConfig.Init();
        instance = this;
    }
    
    private void Start()
    {     
        UpdateState();
    }
}
