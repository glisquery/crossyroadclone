﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{ 
    public static float volume = 1f;

    public void UpdateVolume()
    {
        GetComponent<AudioSource>().volume = volume;
    }

    public void Play(string path)
    {
        GetComponent<Poolable>().OnCreate.AddListener(OnCreate);

        var clip = Resources.Load<AudioClip>("Sounds/" + path);
        var audioSource = GetComponent<AudioSource>();
        audioSource.volume = volume;
        audioSource.clip = clip;
        audioSource.Play();
           
        var timerInst = GetComponent<Timer>();
        timerInst.Duration = clip.length;
        timerInst.Run(); 
    }

    private void OnCreate()
    {
        gameObject.AddComponent<Timer>();
        var timerInst = GetComponent<Timer>();
        timerInst.TimerFinishedEvent.AddListener(OnClipFinished);
    }

    private void OnClipFinished()
    {
        GetComponent<Poolable>().Remove();
    }
}
