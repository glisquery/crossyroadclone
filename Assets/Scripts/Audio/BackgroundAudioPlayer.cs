﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAudioPlayer : MonoBehaviour
{
    [SerializeField]
    private Timer timerPrefab; 
    private Timer cityCarsTimer;
    private AudioManager audioManager;

    private void Start()
    {
        InitialSetup();
    }

    private void Update()
    {
        if (!audioManager)
        {
            audioManager = AudioManager.Instance;
        }
    }

    private void InitialSetup()
    {
        audioManager = AudioManager.Instance;
        var cityCarsClip = Resources.Load<AudioClip>("Sounds/" + "CityCars");
        cityCarsTimer = Instantiate(timerPrefab);
        cityCarsTimer.Duration = cityCarsClip.length;
        cityCarsTimer.TimerFinishedEvent.AddListener(PlayCityCarsClip);
        cityCarsTimer.Run();
        if (audioManager)
        {
            audioManager.Play("CityCars");
        }
    }

    private void PlayCityCarsClip()
    {
        audioManager.Play("CityCars");
        cityCarsTimer.Run();
    }
}
