﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config
{
    public float cellSize;
    public int gridWidth;
    public int gridHeight;
    public int railwayNumTracksMin;
    public int railwayNumTracksMax;
    public int roadNumTracksMin;
    public int roadNumTracksMax;
    public int highwayNumTracksMin;
    public int highwayNumTracksMax;
    public int footpathNumTracksMin;
    public int footpathNumTracksMax;
    public int riverNumTracksMin;
    public int riverNumTracksMax;
    public float railwayProb;
    public float roadProb;
    public float highwayProb;
    public float riverProb;
    public float footpathProb;
    public float carSpawnTimeoutMin;
    public float carSpawnTimeoutMax;
    public float truckSpawnTimeoutMin;
    public float truckSpawnTimeoutMax;
    public float trainSpawnTimeoutMin;
    public float trainSpawnTimeoutMax;
    public float timberSpawnTimeoutMin;
    public float timberSpawnTimeoutMax;
    public int lilySpawnCntMin;
    public int lilySpawnCntMax;
    public int fenceDist;
    public int forestSpawnItemCntMin;
    public int forestSpawnItemCntMax;
    public bool mute;
    public int coins;
    public int record;
}
