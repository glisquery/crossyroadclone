﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using UnityEngine;

public class ConfigData : System.ICloneable
{
    private Dictionary<string, string> property;
    private static CultureInfo cultureInfo = new CultureInfo("en-US");

    public ConfigData()
    {
        property = new Dictionary<string, string>();
    }

    public object Clone()
    {
        return this.MemberwiseClone();
    }
    
    public void Add(string name, string value)
    {
        if (!Has(name))
        {
            property.Add(name, value);
        }
    }

    public void Set(string name, string value)
    {
        Add(name, value);
        property[name] = value;
    }

    public T Get<T>(string name)
    {
        var ret = default(T);
        var converter = TypeDescriptor.GetConverter(typeof(T));
        if (converter.CanConvertFrom(typeof(string)))
        {
            string value = property[name];
            ret = (T)converter.ConvertFromString(
                null,
                cultureInfo,
                value
            );
        }

        return ret;
    }

    public bool Has(string name)
    {
        return property.ContainsKey(name);
    }
}
