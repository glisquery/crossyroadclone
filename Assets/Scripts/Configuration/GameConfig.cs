﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class GameConfig
{
    private static Config config = new Config();
    private static ConfigData actualConfig;
    private static ConfigData fallbackConfig;
    private static bool isInitialized = false;
    
    public static Config Actual
    {
        get
        {
            return config;
        }
    }
    
    public static ConfigData Config
    {
        get
        {
            return actualConfig;
        }
    }

    public static ConfigData Fallback
    {
        get
        {
            return fallbackConfig.Clone() as ConfigData;
        }
    }

    public static void Sync()
    {
        SyncConfigData();
    }

    public static void Save()
    {
        SetPropertyMute();
        SetPropertyCoins();
        SetPropertyRecord();
        PlayerPrefs.Save();
    }

    public static void Load()
    {
        LoadPropertyMute();
        LoadPropertyCoins();
        LoadPropertyRecord();
    }

    public static void Init()
    {
        if (isInitialized)
        {
            return;
        }

        InitFallbackConfig();
        actualConfig = fallbackConfig.Clone() as ConfigData;
        LoadActualConfig();
        SyncConfigData();

        isInitialized = true;
    }

    private static void InitFallbackConfig()
    {
        fallbackConfig = new ConfigData();
        fallbackConfig.Add("cellSize", "1");
        fallbackConfig.Add("gridWidth", "30");
        fallbackConfig.Add("gridHeight", "40");
        fallbackConfig.Add("railwayNumTracksMin", "1");
        fallbackConfig.Add("railwayNumTracksMax", "1");
        fallbackConfig.Add("roadNumTracksMin", "1");
        fallbackConfig.Add("roadNumTracksMax", "1");
        fallbackConfig.Add("highwayNumTracksMin", "1");
        fallbackConfig.Add("highwayNumTracksMax", "3");
        fallbackConfig.Add("footpathNumTracksMin", "1");
        fallbackConfig.Add("footpathNumTracksMax", "4");
        fallbackConfig.Add("riverNumTracksMin", "1");
        fallbackConfig.Add("riverNumTracksMax", "2");
        fallbackConfig.Add("railwayProb", "0.1");
        fallbackConfig.Add("roadProb", "0.3");
        fallbackConfig.Add("highwayProb", "0.2");
        fallbackConfig.Add("riverProb", "0.1");
        fallbackConfig.Add("footpathProb", "0.3");
        fallbackConfig.Add("carSpawnTimeoutMin", "1.5");
        fallbackConfig.Add("carSpawnTimeoutMax", "2.5");
        fallbackConfig.Add("truckSpawnTimeoutMin", "1");
        fallbackConfig.Add("truckSpawnTimeoutMax", "3");
        fallbackConfig.Add("trainSpawnTimeoutMin", "6");
        fallbackConfig.Add("trainSpawnTimeoutMax", "12");
        fallbackConfig.Add("timberSpawnTimeoutMin", "1.8");
        fallbackConfig.Add("timberSpawnTimeoutMax", "2.2");
        fallbackConfig.Add("lilySpawnCntMin", "2");
        fallbackConfig.Add("lilySpawnCntMax", "6");
        fallbackConfig.Add("fenceDist", "14");
        fallbackConfig.Add("forestSpawnItemCntMin", "0");
        fallbackConfig.Add("forestSpawnItemCntMax", "6");
        fallbackConfig.Add("mute", "false");
        fallbackConfig.Add("coins", "0");
        fallbackConfig.Add("record", "0");
    }

    private static void LoadActualConfig()
    {
        Load();
    }

    private static void SyncConfigData()
    {
        System.Object configObject = config;
        FieldInfo[] fields = configObject.GetType().GetFields();
        foreach (FieldInfo fieldInfo in fields)
        {
            var fieldType = fieldInfo.FieldType;
            MethodInfo method = typeof(ConfigData).GetMethod("Get");
            method = method.MakeGenericMethod(fieldType);
            System.Object fieldValue = method.Invoke(
                actualConfig,
                new object[] {fieldInfo.Name}
            );
            fieldInfo.SetValue(configObject, fieldValue);
        }
    }

    private static void LoadPropertyMute()
    {
        if (!PlayerPrefs.HasKey("mute"))
        {
            return;
        }

        actualConfig.Set("mute", PlayerPrefs.GetString("mute"));
    }

    private static void LoadPropertyCoins()
    {
        if (!PlayerPrefs.HasKey("coins"))
        {
            return;
        }

        actualConfig.Set("coins", PlayerPrefs.GetString("coins"));
    }

    private static void LoadPropertyRecord()
    { 
        if (!PlayerPrefs.HasKey("record"))
        {
            return;
        }

        actualConfig.Set("record", PlayerPrefs.GetString("record"));
    }

    private static void SetPropertyMute()
    { 
        PlayerPrefs.SetString("mute", actualConfig.Get<string>("mute"));
    }

    private static void SetPropertyCoins()
    { 
        PlayerPrefs.SetString("coins", actualConfig.Get<string>("coins"));
    }

    private static void SetPropertyRecord()
    { 
        PlayerPrefs.SetString("record", actualConfig.Get<string>("record"));
    }
}
