﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class Character : MonoBehaviour
{
    private static Character instance = null;
    private static readonly object padlock = new object();

    private static int mapWidth = 0;
    private static float cellSize = 0f;
    private static GameObject nearestTimber = null;
    private static GameObject nearestLily = null;
    private static Vector3 newPos = new Vector3(0f, 0.35f, 0f);
    private static bool attached = false;
    private static Vector3 scale = new Vector3(1f, 1f, 1f);
    
    private UnityEvent killedEvent = new UnityEvent();
    private UnityEvent wantToDieEvent = new UnityEvent();
    private UnityEvent coinPickupEvent = new UnityEvent();
    private UnityEvent moveEvent = new UnityEvent();
    private bool killed = false;
    private bool goneCrazy = false;
    private float moveTime = 0.2f;
    private AudioManager audioManager = null;

    public static Character Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    }

    public static Vector3 Pos
    {
        get
        {
            return newPos;
        }
    }

    public UnityEvent KilledEvent
    {
        get
        {
            return killedEvent;
        }
    }

    public UnityEvent WantToDieEvent
    {
        get
        {
            return wantToDieEvent;
        }
    }

    public UnityEvent CoinPickupEvent
    {
        get
        {
            return coinPickupEvent;
        }
    }

    public UnityEvent MoveEvent
    {
        get
        {
            return moveEvent;
        }
    }

    public void Awake()
    {
        instance = this;
    }

    public void MoveLeft()
    {
        if (attached)
        {
            newPos = transform.position;
            newPos.y = 0.35f;
        } 

        var posTo = newPos + new Vector3(-cellSize, 0f, 0f);

        if (!CanMove(newPos, posTo))
        {
            return;
        }

        transform.DORotate(
            new Vector3(0f, -90f, 0f),
            0.2f,
            RotateMode.Fast
        );
        MoveCharacter(posTo);
    }

    public void MoveRight()
    {
        if (attached)
        {
            newPos = transform.position;
            newPos.y = 0.35f;
        } 

        var posTo = newPos + new Vector3(cellSize, 0f, 0f);

        if (!CanMove(newPos, posTo))
        {
            return;
        }

        transform.DORotate(
            new Vector3(0f, 90f, 0f),
            0.2f,
            RotateMode.Fast
        );
        MoveCharacter(posTo);
    }

    public void MoveForward()
    {
        if (attached)
        {
            newPos = transform.position;
            newPos.y = 0.35f;
        } 

        var posTo = newPos + new Vector3(0f, 0f, 2f * cellSize);

        if (!CanMove(newPos, posTo))
        {
            return;
        }

        transform.DORotate(
            new Vector3(0f, 0f, 0f),
            0.2f,
            RotateMode.Fast
        );
        MoveCharacter(posTo);
    }

    public void MoveBack()
    {
        if (attached)
        {
            newPos = transform.position;
            newPos.y = 0.35f;
        } 
        var posTo = newPos + new Vector3(0f, 0f, -2f * cellSize);

        if (!CanMove(newPos, posTo))
        {
            return;
        }

        transform.DORotate(
            new Vector3(0f, 180f, 0f),
            0.2f,
            RotateMode.Fast
        );
        MoveCharacter(posTo);
    }

    private void MoveCharacter(Vector3 posTo)
    {
        newPos = transform.position;
        newPos.y = 0.35f;
        transform.parent = null;
        transform.position = new Vector3(
            transform.position.x,
            0.35f,
            transform.position.z
        );

        if (HitsTimber(newPos, posTo))
        {
            MoveToTimber(posTo);
        }
        else if (HitsLily(newPos, posTo))
        {
            MoveToLily(posTo);
        }
        else
        {
            MoveSimple(posTo);
        }
    }

    private bool HitsTimber(Vector3 posFrom, Vector3 posTo)
    {
        var ret = false;
        var dir = Vector3.down;
        var dist = cellSize * 20f;
        RaycastHit hitInfo;

        var hit = Physics.Raycast(
            posTo,
            dir,
            out hitInfo,
            dist
        );
        if (hit)
        {
            var otherObj = hitInfo.collider.gameObject;
            ret = otherObj.tag == "Timber";
            if (ret)
            {
                nearestTimber = otherObj;
            }
            else
            {
                nearestTimber = null;
            }

            return ret;
        }

        hit = Physics.Raycast(
            posTo + new Vector3(cellSize / 2f, 0f, 0f),
            dir,
            out hitInfo,
            dist
        );
        if (hit)
        {
            var otherObj = hitInfo.collider.gameObject;
            ret = otherObj.tag == "Timber";
            if (ret)
            {
                nearestTimber = otherObj;
            }
            else
            {
                nearestTimber = null;
            }

            return ret;
        }

        hit = Physics.Raycast(
            posTo - new Vector3(cellSize / 2f, 0f, 0f),
            dir,
            out hitInfo,
            dist
        );
        if (hit)
        {
            var otherObj = hitInfo.collider.gameObject;
            ret = otherObj.tag == "Timber";
            if (ret)
            {
                nearestTimber = otherObj;
            }
            else
            {
                nearestTimber = null;
            }

            return ret;
        }

        return ret;
    }

    private bool HitsLily(Vector3 posFrom, Vector3 posTo)
    {
        var ret = false;
        var dir = Vector3.down;
        var dist = cellSize * 20f;
        RaycastHit hitInfo;

        var hit = Physics.Raycast(
            posTo,
            dir,
            out hitInfo,
            dist
        );
        if (hit)
        {
            var otherObj = hitInfo.collider.gameObject;
            ret = otherObj.tag == "Lily";
            if (ret)
            {
                nearestLily = otherObj;
            }
            else
            {
                nearestLily = null;
            }

            return ret;
        }

        hit = Physics.Raycast(
            posTo + new Vector3(cellSize / 2f, 0f, 0f),
            dir,
            out hitInfo,
            dist
        );
        if (hit)
        {
            var otherObj = hitInfo.collider.gameObject;
            ret = otherObj.tag == "Lily";
            if (ret)
            {
                nearestLily = otherObj;
            }
            else
            {
                nearestLily = null;
            }

            return ret;
        }

        hit = Physics.Raycast(
            posTo - new Vector3(cellSize / 2f, 0f, 0f),
            dir,
            out hitInfo,
            dist
        );
        if (hit)
        {
            var otherObj = hitInfo.collider.gameObject;
            ret = otherObj.tag == "Lily";
            if (ret)
            {
                nearestLily = otherObj;
            }
            else
            {
                nearestLily = null;
            }

            return ret;
        }

        return ret;
    }


    private void MoveSimple(Vector3 posTo)
    {
        var cellX = GameMap.GetCellX(posTo);
        var cellY = GameMap.GetCellY(posTo);
        var pos = GameMap.GetCellCenter(cellX, cellY);
        attached = false;
        newPos = new Vector3(pos.x, posTo.y, pos.y); 
        transform
            .DOJump(newPos, 0.4f, 1, moveTime, false)
            .OnComplete(UpdatePosition);   
    }

    private void MoveToTimber(Vector3 posTo)
    {
        if (nearestTimber)
        {
            var nextPos = new Vector3(
                newPos.x + (posTo.x - newPos.x) / 2f,
                posTo.y,
                posTo.z
            );
            nextPos.y = transform.position.y;
            transform.position = nextPos;
            transform
                .DOJump(nextPos, 0.4f, 1, moveTime, false);
            transform.parent = nearestTimber.gameObject.transform;
            attached = true;
            audioManager.Play("CharStepToTimber");
        }
    }

    private void MoveToLily(Vector3 posTo)
    {
        if (nearestLily)
        {
            nearestLily.gameObject.GetComponent<LilyAnimator>().Punch();
            var cellX = GameMap.GetCellX(posTo);
            var cellY = GameMap.GetCellY(posTo);
            var pos = GameMap.GetCellCenter(cellX, cellY);
            attached = false;
            newPos = new Vector3(pos.x, posTo.y, pos.y); 
            transform.DOJump(newPos, 0.4f, 1, moveTime, false);
            audioManager.Play("CharStepToLily");
        }
    }

    private bool CanMove(Vector3 posFrom, Vector3 posTo)
    {
        if ((((posTo - posFrom) == Vector3.left) ||
             ((posTo - posFrom) == Vector3.right)) &&
            (Mathf.Abs(posTo.x) > mapWidth * cellSize / 2.7f))
        {
            return false;
        }

        var ret = true;
        var dir = posTo - posFrom;
        var dist = dir.magnitude;
        dir = dir.normalized;
        RaycastHit hitInfo;

        var hit = Physics.Raycast(
            posFrom,
            dir,
            out hitInfo,
            dist
        );
        if (hit)
        {
            ret = false;
            var otherObj = hitInfo.collider.gameObject;
            ret = !(otherObj.tag == "Fence");
        }

        return ret;
    }

    private void AfterMove()
    {
        var waterCollision = false;
        var dir = Vector3.down;
        var dist = 100f;
        var posFrom = transform.position;
        RaycastHit hitInfo;

        var hit = Physics.Raycast(
            posFrom,
            dir,
            out hitInfo,
            dist
        );
        if (hit)
        {
            var otherObj = hitInfo.collider.gameObject;
            waterCollision = otherObj.tag == "Water";
        }

        if (waterCollision)
        {
            if (!killed)
            {
                killedEvent.Invoke();
                killed = true;
                audioManager.Play("CharStepToWater");
                audioManager.Play("CharDeath");
            }
        }
        else
        {
            audioManager.Play("CharStepDefault");
        }
    }

    private void Start()
    {
        GameConfig.Init();
        scale = transform.localScale;
        audioManager = AudioManager.Instance;
        cellSize = GameConfig.Actual.cellSize;
        mapWidth = GameConfig.Actual.gridWidth;
        transform.position = new Vector3(0f, 0.35f, 8f);
        newPos = transform.position;
        UpdatePosition();
    }

    private void Update()
    {
        transform.localScale = scale;
        var curCellX = GameMap.GetCellX(transform.position);
        var curCellY = GameMap.GetCellY(transform.position);
        if (Mathf.Abs(transform.position.x) > mapWidth * cellSize / 1.5f)
        {
            InputManager.DisableInput();
            killed = true;
            killedEvent.Invoke();
        }
        if ((!killed) && (!goneCrazy))
        {
            if (Mathf.Abs(transform.position.x) > mapWidth * cellSize / 3f)
            {
                wantToDieEvent.Invoke();
                goneCrazy = true;
            }
        }
    }

    private void UpdatePosition()
    {
        if (transform)
        {
            newPos = transform.position;
            GameMap.CharacterPosition = transform.position;
            MoveEvent.Invoke();
            AfterMove();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!killed)
        {
            if (GetComponent<Collider>().gameObject.tag == "Vehicle")
            {
                killedEvent.Invoke();
                killed = true;
            }

        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (!killed)
        {
            if (collider.gameObject.tag == "Vehicle")
            {
                killedEvent.Invoke();
                killed = true;
                audioManager.Play("CharDeath");
            }
            if (collider.gameObject.tag == "Coin")
            {
                coinPickupEvent.Invoke();
                audioManager.Play("CoinPickup");
                var coin = collider.gameObject;
                coin.GetComponent<Poolable>().Remove();
            }
        }
    }
}
