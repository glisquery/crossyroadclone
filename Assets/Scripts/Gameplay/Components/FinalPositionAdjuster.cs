﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalPositionAdjuster : MonoBehaviour
{
    public virtual Vector3 Adjust(Vector3 pos)
    {
        return pos;
    }
}
