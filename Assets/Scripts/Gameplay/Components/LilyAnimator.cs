﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LilyAnimator : MonoBehaviour
{
    public void Punch()
    {
        transform.DOPunchPosition(
            Vector3.down * 0.08f,
            0.4f,
            3,
            0.1f,
            false
        );
    }
}
    
