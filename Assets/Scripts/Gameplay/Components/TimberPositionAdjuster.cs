﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TimberPositionAdjuster : FinalPositionAdjuster
{
    public float timberLen = 0f;
    public int timberWidth = 0;

    private bool allowPunch = true;
    private Vector3[] candidates = null;

    public override Vector3 Adjust(Vector3 pos)
    {
        Animate();
        Process();

        return Nearest(pos);
    }

    private void Animate()
    {
        if (!allowPunch)
        {
            return;
        }

        allowPunch = false;
        
        transform.DOPunchPosition(
            Vector3.down * 0.2f,
            0.2f,
            3,
            0.4f,
            false
        )
        .OnComplete(
           AllowPunch
        );
    }

    private void AllowPunch()
    {
        allowPunch = true;
    }

    private void Process()
    {
        candidates = new Vector3[timberWidth];
        var center = transform.position;
        var left = (
            center -
            new Vector3(timberLen * timberWidth / 2f + timberLen / 2f, 0f, 0f)
        );
        for (var i = 0; i < timberWidth; i++)
        {
            candidates[i] = left + new Vector3(i * timberLen, 0f, 0f);
        }
    }

    private Vector3 Nearest(Vector3 pos)
    {
        var ret = 0;
        for (var i = 1; i < timberWidth; i++)
        {
            var dist = (pos - candidates[i]).magnitude;
            if (dist < (pos - candidates[ret]).magnitude)
            {
                ret = i;
            }
        }

        return candidates[ret];
    }
}
