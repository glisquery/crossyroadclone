﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameInitializer : MonoBehaviour
{
    [SerializeField]
    private GameMap gameMap;
    [SerializeField]
    private GameStateManager gameStateManager;
    [SerializeField]
    private InputManager inputManager;
    [SerializeField]
    private PlayerStatManager playerStatManager;
    [SerializeField]
    private AudioManager audioManager;
    [SerializeField]
    private BackgroundAudioPlayer backgroundAudioPlayer;
    [SerializeField]
    private Character characterPrefab;
     
    private void Start()
    {
        Init();
    }

    private void Init()
    {
        InitSettings();
        GameConfig.Init();
        InitDOTween();
        Instantiate(gameMap);
        Instantiate(gameStateManager);
        Instantiate(inputManager);
        Instantiate(playerStatManager);
        Instantiate(audioManager);
        Instantiate(characterPrefab);
        Instantiate(backgroundAudioPlayer);
    }

    private void InitSettings()
    {
        Application.targetFrameRate = 60;
    }

    private void InitDOTween()
    {
        DOTween
            .Init(false, true, LogBehaviour.ErrorsOnly)
            .SetCapacity(5000, 1000);
    }
}
