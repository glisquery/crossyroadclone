﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

public class GameStateManager : MonoBehaviour
{
    private static GameStateManager instance;
    private static readonly object padlock = new object();

    private UnityEvent gameOverEvent = new UnityEvent();
    private UnityEvent panicModeEvent = new UnityEvent();
    private Character characterInstance = null;

    public static GameStateManager Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    }

    public UnityEvent GameOverEvent
    {
        get
        {
            return gameOverEvent;
        }
    }

    public UnityEvent PanicModeEvent
    {
        get
        {
            return panicModeEvent;
        }
    }
    
    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        characterInstance = Character.Instance;
        if (characterInstance)
        {
            characterInstance.KilledEvent.AddListener(OnCharacterKilled);
        } 
    }

    private void OnCharacterKilled()
    {
        gameOverEvent.Invoke();
    }
}
