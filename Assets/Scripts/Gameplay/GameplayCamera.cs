﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameplayCamera : MonoBehaviour
{
    private static GameplayCamera instance = null;
    private static readonly object padlock = new object();
    
    private Vector3 reservedPos = Vector3.zero;

    public static bool Locked { get; set; }

    public static GameplayCamera Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    } 
    
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Locked = false;
        transform.position = new Vector3(
            transform.position.x,
            transform.position.y,
            5f
        );
    }

    private void Update()
    {
        var charInstance = Character.Instance;

        if ((Locked) || (!charInstance))
        {
            return;
        }

        var charPos = charInstance.transform.position;

        if (charPos == new Vector3(0f, 0.35f, 0f))
        {
            return;
        }

        if (charPos.z - transform.position.z < -1f)
        {
            GameStateManager.Instance.GameOverEvent.Invoke();            
        }

        var adjust = false; 
        var newCameraPos = charInstance.transform.position;
        newCameraPos = transform.position;
        newCameraPos += new Vector3(0, 0f, 0.02f);
        newCameraPos.y = transform.position.y;

        if (Mathf.Abs(charPos.x - newCameraPos.x + 1f) > 1.01f)
        {
            newCameraPos.x = charInstance.transform.position.x + 1f - 0.05f;
            adjust = true;
        }

        if (charPos.z - newCameraPos.z > 6f)
        {
            adjust = true;
            newCameraPos.z = charInstance.transform.position.z - 5.4f;
        }

        if (adjust)
        {
            newCameraPos += new Vector3(0f, 0f, 0.4f);
            transform.DOMove(newCameraPos, 1.5f, false);
        }
        else
        {
            transform.position = newCameraPos;
        }
    }
}
