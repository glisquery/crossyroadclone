﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;

public class GameMap : MonoBehaviour
{
    private static GameMap instance;
    private static readonly object padlock = new object();

    public static Mutex layerSpawnLock = new Mutex();

    private static Vector3 characterPosition = Vector3.zero;
    private static System.Random rnd;
    private static Mutex edge = new Mutex();
    private static float mapEdge = 0f;
    private static int mapWidth = 0;
    private static int mapHeight = 0;
    private static float cellSize = 0f; 
    
    [SerializeField]
    private FootpathLayerFactory footpathLayerFactoryPrefab;
    [SerializeField]
    private RiverLayerFactory riverLayerFactoryPrefab;
    [SerializeField]
    private RoadLayerFactory roadLayerFactoryPrefab;
    [SerializeField]
    private HighwayLayerFactory highwayLayerFactoryPrefab;
    [SerializeField]
    private RailwayLayerFactory railwayLayerFactoryPrefab;
    private UnityEvent footpathLayerSpawnRequestEvent = new UnityEvent();
    private UnityEvent riverLayerSpawnRequestEvent = new UnityEvent();
    private UnityEvent roadLayerSpawnRequestEvent = new UnityEvent();
    private UnityEvent highwayLayerSpawnRequestEvent = new UnityEvent();
    private UnityEvent railwayLayerSpawnRequestEvent = new UnityEvent();
    private bool riverIsReady = false;
    private bool footpathIsReady = false;
    private bool roadIsReady = false;
    private bool highwayIsReady = false;
    private bool railwayIsReady = false;
    private bool started = false;
    private UnityEvent[] spawnRequests;

    public static GameMap Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    }
 

    static GameMap()
    {
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    public static float MapEdge
    {
        get
        {
            edge.WaitOne();
            var ret = mapEdge;
            edge.ReleaseMutex();

            return ret;
        }
        set
        {
            edge.WaitOne();
            mapEdge = Mathf.Max(value, mapEdge);
            edge.ReleaseMutex();
        }
    }

    public static Vector3 CharacterPosition
    {
        get
        {
            return characterPosition;
        }
        set
        {
            characterPosition = value;
        }
    }

    public UnityEvent FootpathLayerSpawnRequestEvent
    {
        get
        {
            return footpathLayerSpawnRequestEvent;
        }
    }
  
    public UnityEvent RiverLayerSpawnRequestEvent
    {
        get
        {
            return riverLayerSpawnRequestEvent;
        }
    }

    public UnityEvent RoadLayerSpawnRequestEvent
    {
        get
        {
            return roadLayerSpawnRequestEvent;
        }
    }

    public UnityEvent HighwayLayerSpawnRequestEvent
    {
        get
        {
            return highwayLayerSpawnRequestEvent;
        }
    }

    public UnityEvent RailwayLayerSpawnRequestEvent
    {
        get
        {
            return railwayLayerSpawnRequestEvent;
        }
    }

    public static int GetCellX(Vector3 pos)
    {
        return Mathf.FloorToInt(pos.x + cellSize / 2f);
    }

    public static int GetCellY(Vector3 pos)
    {
        return Mathf.FloorToInt(pos.z + cellSize / 2f);
    }

    public static Vector2 GetCellCenter(int x, int y)
    {
        return new Vector2(x * cellSize, y * cellSize);
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Init();
        Instantiate(footpathLayerFactoryPrefab);
        Instantiate(riverLayerFactoryPrefab);
        Instantiate(roadLayerFactoryPrefab);
        Instantiate(highwayLayerFactoryPrefab);
        Instantiate(railwayLayerFactoryPrefab);
        SubscribeToFactories();
    }

    private void Init()
    {
        mapEdge = 0;
        mapWidth = GameConfig.Actual.gridWidth;
        mapHeight = GameConfig.Actual.gridHeight;
        cellSize = GameConfig.Actual.cellSize;
        spawnRequests = new UnityEvent[]
        {
            footpathLayerSpawnRequestEvent,
            riverLayerSpawnRequestEvent,
            roadLayerSpawnRequestEvent,
            highwayLayerSpawnRequestEvent,
            railwayLayerSpawnRequestEvent
        };
    }

    private void SubscribeToFactories()
    {
        var footpathFactory = FootpathLayerFactory.Instance;
        if (footpathFactory)
        {
            footpathFactory
                .LayerFactoryReadyEvent
                .AddListener(ActivateFootpathFactory);
        } 
        var riverFactory = RiverLayerFactory.Instance;
        if (riverFactory)
        {
            riverFactory
                .LayerFactoryReadyEvent
                .AddListener(ActivateRiverFactory);
        } 
        var roadFactory = RoadLayerFactory.Instance;
        if (roadFactory)
        {
            roadFactory
                .LayerFactoryReadyEvent
                .AddListener(ActivateRoadFactory);
        } 
        var highwayFactory = HighwayLayerFactory.Instance;
        if (highwayFactory)
        {
            highwayFactory
                .LayerFactoryReadyEvent
                .AddListener(ActivateHighwayFactory);
        }
        var railwayFactory = RailwayLayerFactory.Instance;
        if (railwayFactory)
        {
            railwayFactory
                .LayerFactoryReadyEvent
                .AddListener(ActivateRailwayFactory);
        }
    }

    private void ActivateFootpathFactory()
    {
        footpathIsReady = true;
        var factory = FootpathLayerFactory.Instance;
        factory.LayerFactoryReadyEvent.RemoveListener(ActivateFootpathFactory); 
    }

    private void ActivateRiverFactory()
    {
        riverIsReady = true;
        var factory = RiverLayerFactory.Instance;
        factory.LayerFactoryReadyEvent.RemoveListener(ActivateRiverFactory); 
    }

    private void ActivateRoadFactory()
    {
        roadIsReady = true;
        var factory = RoadLayerFactory.Instance;
        factory.LayerFactoryReadyEvent.RemoveListener(ActivateRoadFactory); 
    }

    private void ActivateHighwayFactory()
    {
        highwayIsReady = true;
        var factory = HighwayLayerFactory.Instance;
        factory.LayerFactoryReadyEvent.RemoveListener(ActivateHighwayFactory); 
    }

    private void ActivateRailwayFactory()
    {
        railwayIsReady = true;
        var factory = RailwayLayerFactory.Instance;
        factory.LayerFactoryReadyEvent.RemoveListener(ActivateRailwayFactory); 
    }

    private bool FactoriesReady()
    {
        return (
            (footpathIsReady) &&
            (riverIsReady) &&
            (roadIsReady) &&
            (highwayIsReady) &&
            (railwayIsReady)
        );
    }

    private void Update()
    {
        if (!started)
        {
            if (!FactoriesReady())
            {
                return;
            }

            MapEdge = -cellSize * 20;
            for (var i = 0; i < 8; i++)
            {
                spawnRequests[0].Invoke();
            }
            started = true;
            var characterInstance = Character.Instance;
        }
        if (FactoriesReady())
        {
            var characterInstance = Character.Instance;

            if (!characterInstance)
            {
                return;
            }

            var characterPos = characterInstance.transform.position.z;
            
            if (characterPos + cellSize * mapHeight * 0.9 > mapEdge)
            {
                var eventNum = rnd.Next(spawnRequests.Length);
                spawnRequests[eventNum].Invoke();
            }
        }
    }
}
