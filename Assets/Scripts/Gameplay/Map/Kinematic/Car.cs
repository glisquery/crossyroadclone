﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Car : MonoBehaviour
{
    public Vector3 velocity = Vector3.right;
    public float speed = 3f;

    private Timer lifeTimer;

    public void Run()
    {
        var width = GameConfig.Actual.gridWidth;
        var cellSize = GameConfig.Actual.cellSize;
        var rb = GetComponent<Rigidbody>();
        var curPos = transform.position;
        var newPos = transform.position + velocity * width * cellSize * 2;
        var dist = (newPos - curPos).magnitude;
        var time = dist / speed;
        transform.DOMove(newPos, time, false).OnComplete(Destroy); 
    }
     
    private void Start()
    {
        lifeTimer = gameObject.AddComponent<Timer>();
    }

    private void Destroy()
    {
        GetComponent<Poolable>().Remove();
    }
}
