﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour
{
    private bool updateLocked = true;
    private float[] spawnPoints;
    private int[] dir;
    private Timer timer;
    private int curPoint = 0;
    private float timeoutMin = 0f;
    private float timeoutMax = 0f;
    private float speed = 3f;

    private static Vector3[] dirVec;
    private static float[] dirAngle;
    private static float[] dirShift;
    private static System.Random rnd;
    
    static CarSpawner()
    {
        dirVec = new Vector3[]
        {
            Vector3.left,
            Vector3.right
        };
        dirAngle = new float[]
        {
            90f,
            -90f
        };
        dirShift = new float[]
        {
            1f,
            -1f
        };
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    public void Unlock()
    {
        updateLocked = false;
    }

    public void SetSpawnPoints(float[] points)
    {
        spawnPoints = points;
        dir = new int[points.Length];
        for (int i = 0; i < points.Length; i++)
        {
            dir[i] = rnd.Next(2);
        }
    }
    
    public void Startup()
    { 
        timeoutMin = GameConfig.Actual.carSpawnTimeoutMin;
        timeoutMax = GameConfig.Actual.carSpawnTimeoutMax; 
        curPoint = 0;
        updateLocked = true;
        SetupTimer();
    }

    public void Stop()
    {
        updateLocked = true;
        curPoint = 0;
    }

    private void Update()
    {
        if (updateLocked)
        {
            return;
        }
    }

    private void SetupTimer()
    {
        timer = gameObject.AddComponent<Timer>() as Timer;
        var timeout = Random.Range(timeoutMin, timeoutMax);
        timer.Duration = timeout;
        timer.TimerFinishedEvent.AddListener(SpawnNextCar);
        timer.Run();
    }

    private void SpawnNextCar()
    {
        if (!updateLocked)
        {
            CreateCar();
        }
        var timeout = Random.Range(timeoutMin, timeoutMax);
        timer.Duration = timeout;
        timer.Run();
    }

    private void CreateCar()
    {
        GameObject car = null;
        var carType = rnd.Next(2);
        if (carType == 1)
        {
            car = Pool.Get("CarType2");
        }
        else
        {
            car = Pool.Get("CarType1");
        }
        var carInstance = car.GetComponent<Car>();
        carInstance.velocity = dirVec[dir[curPoint]];
        var character = Character.Instance;
        var curPos = car.transform.position;
        var characterPos = GameMap.CharacterPosition;
        var curRot = car.transform.eulerAngles;
        var angle = dirAngle[dir[curPoint]];
        var mapWidth = GameConfig.Actual.gridWidth;
        var cellSize = GameConfig.Actual.cellSize;
        car.transform.position = new Vector3(
            mapWidth * cellSize * dirShift[dir[curPoint]],
            curPos.y,
            spawnPoints[curPoint]
        );
        car.transform.eulerAngles = new Vector3(curRot.x, angle, curRot.z);
        carInstance.speed = speed;
        carInstance.Run();
        curPoint = (curPoint + 1) % spawnPoints.Length;
    }
}
