﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Lily : MonoBehaviour
{
    public Vector3 velocity = Vector3.right;

    private Timer lifeTimer;

    private void Start()
    {
        gameObject.AddComponent<LilyAnimator>();
    }

    private void Destroy()
    {
        GetComponent<Poolable>().Remove();
    }
}
