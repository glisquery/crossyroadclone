﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LilySpawner : MonoBehaviour
{
    private bool updateLocked = true;
    private float[] spawnPoints;
    private int[] dir;
    private Timer timer;
    private int curPoint = 0;
    private ArrayList lilies = new ArrayList();

    private static Vector3[] dirVec;
    private static float[] dirAngle;
    private static float[] dirShift;
    private static System.Random rnd;
    
    static LilySpawner()
    {
        dirVec = new Vector3[]
        {
            Vector3.left,
            Vector3.right
        };
        dirAngle = new float[]
        {
            90f,
            -90f
        };
        dirShift = new float[]
        {
            1f,
            -1f
        };
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    public void Unlock()
    {
        updateLocked = false;
    }

    public void SetSpawnPoints(float[] points)
    {
        spawnPoints = points;
        dir = new int[points.Length];
        for (int i = 0; i < points.Length; i++)
        {
            dir[i] = rnd.Next(2);
        }
    }
    
    private void Start()
    { 
        curPoint = 0;
        updateLocked = true;
        SetupTimer();
    }

    private void Update()
    {
        if (updateLocked)
        {
            return;
        }
    }

    private void SetupTimer()
    {
       SpawnNextLily();
    }

    private void SpawnNextLily()
    {
        CreateLilies();
    }

    private void CreateLilies()
    {
        if (spawnPoints.Length == 0)
        {
            return;
        }

        var character = Character.Instance;
        var characterPos = GameMap.CharacterPosition;             
        var mapWidth = GameConfig.Actual.gridWidth;
        var cellSize = GameConfig.Actual.cellSize;
        var maxCnt = GameConfig.Actual.lilySpawnCntMax;
        var minCnt = GameConfig.Actual.lilySpawnCntMin;
        var spawnCnt = rnd.Next(minCnt, maxCnt + 1);
        float[] spawnPositions = new float[maxCnt];
        float initialGenPos = characterPos.x - cellSize;
        for (int i = 0; i < maxCnt; i++)
        {
            spawnPositions[i] = initialGenPos + cellSize * i;
        }
        float[] shuffled = (spawnPositions)
            .OrderBy(x => rnd.Next()).ToArray();    
        for (int i = 0; i < spawnCnt; i++)
        {
            var prevSpawnPoint = curPoint;
            CreateLily(shuffled[i]);
            if (prevSpawnPoint != curPoint)
            {
                CreateLily(shuffled[i]);
            }
        }
    }

    private void CreateLily(float posX)
    {
        GameObject lily = null;
        lily = Pool.Get("Lily");
        lilies.Add(lily);
        var lilyInstance = lily.GetComponent<Lily>();
        lilyInstance.velocity = dirVec[dir[curPoint]];
        var curPos = lily.transform.position;
        var curRot = lily.transform.eulerAngles;
        var angle = dirAngle[dir[curPoint]];
        lily.transform.position = new Vector3(
            posX,
            curPos.y,
            spawnPoints[curPoint]
        );
        lily.transform.eulerAngles = new Vector3(curRot.x, angle, curRot.z);
        curPoint = (curPoint + 1) % spawnPoints.Length;
    }

    private void OnDestroy()
    {
        GameObject[] toDestroy = lilies.ToArray(typeof(GameObject)) as GameObject[];
        foreach (var lily in toDestroy)
        {
            if (lily)
            {
                lily.GetComponent<Poolable>().Remove();
            }
        }
    }
}
