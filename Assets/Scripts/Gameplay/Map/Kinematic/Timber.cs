﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Timber : MonoBehaviour
{
    public Vector3 velocity = Vector3.right;
    public float speed = 0.01f;
    public int width = 0;

    private Timer lifeTimer;
    private int curCellX = 0;
    private int curCellY = 0;
    private float len = 0f;

    public void Run()
    {
        var mapWidth = GameConfig.Actual.gridWidth;
        var cellSize = GameConfig.Actual.cellSize;
        var rb = GetComponent<Rigidbody>();
        var curPos = transform.position;
        var newPos = new Vector3(
            curPos.x + velocity.x * mapWidth * cellSize * 2,
            curPos.y,
            curPos.z
        );
        var dist = (newPos - curPos).magnitude;
        var time = dist / speed;
        len = transform.localScale.z;
        var collider = GetComponent<BoxCollider>();
        var colliderSize = collider.size;
        var scale = transform.localScale;
        transform.localScale = new Vector3(scale.x, scale.y, len * width);
        collider.size = new Vector3(
            2f,
            colliderSize.y,
            2f
        );
        var adjuster = gameObject.AddComponent<TimberPositionAdjuster>();
        adjuster.timberLen = len;
        adjuster.timberWidth = width;
    }
 
    private void Start()
    {
        gameObject.GetComponent<Poolable>().OnCreate.AddListener(OnCreate);
        curCellX = GameMap.GetCellX(transform.position);
        curCellY = GameMap.GetCellY(transform.position);
    }

    private void OnCreate()
    {
        lifeTimer = gameObject.AddComponent<Timer>();
        gameObject.AddComponent<TimberAnimator>();
    }

    private void Update()
    {
        var cellX = GameMap.GetCellX(transform.position);
        var cellY = GameMap.GetCellY(transform.position);
        transform.position += velocity * speed;
    }

    private void Destroy()
    {
        GetComponent<Poolable>().Remove();
    }
}
