﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimberSpawner : MonoBehaviour
{
    private bool updateLocked = true;
    private float[] spawnPoints;
    private int[] dir;
    private Timer timer;
    private int curPoint = 0;

    private static Vector3[] dirVec;
    private static float[] dirAngle;
    private static float[] dirShift;
    private static System.Random rnd;
    
    static TimberSpawner()
    {
        dirVec = new Vector3[]
        {
            Vector3.left,
            Vector3.right
        };
        dirAngle = new float[]
        {
            90f,
            -90f
        };
        dirShift = new float[]
        {
            1f,
            -1f
        };
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    public void Unlock()
    {
        updateLocked = false;
    }

    public void SetSpawnPoints(float[] points)
    {
        spawnPoints = points;
        dir = new int[points.Length];
        for (int i = 0; i < points.Length; i++)
        {
            dir[i] = rnd.Next(2);
        }
    }
    
    private void Start()
    { 
        curPoint = 0;
        updateLocked = true;
        SetupTimer();
    }

    private void Update()
    {
        if (updateLocked)
        {
            return;
        }
    }

    private void SetupTimer()
    {
        timer = gameObject.AddComponent<Timer>() as Timer;
        var timeoutMin = GameConfig.Actual.timberSpawnTimeoutMin;
        var timeoutMax = GameConfig.Actual.timberSpawnTimeoutMax;
        var timeout = Random.Range(timeoutMin, timeoutMax);
        timer.Duration = timeout;
        timer.TimerFinishedEvent.AddListener(SpawnNextTimber);
        timer.Run();
    }

    private void SpawnNextTimber()
    {
        CreateTimber();
        var timeoutMin = GameConfig.Actual.timberSpawnTimeoutMin;
        var timeoutMax = GameConfig.Actual.timberSpawnTimeoutMax;
        var timeout = Random.Range(timeoutMin, timeoutMax);
        timer.Duration = timeout;
        timer.Run();
    }

    private void CreateTimber()
    {
        if (spawnPoints.Length == 0)
        {
            return;
        }

        GameObject timber = null;
        var timberType = rnd.Next(2);
        timber = Pool.Get("Timber");
        var timberInstance = timber.GetComponent<Timber>();
        timberInstance.velocity = dirVec[dir[curPoint]];
        var curPos = timber.transform.position;
        var character = Character.Instance;
        var characterPos = GameMap.CharacterPosition;
        var curRot = timber.transform.eulerAngles;
        var angle = dirAngle[dir[curPoint]];
        var mapWidth = GameConfig.Actual.gridWidth;
        var cellSize = GameConfig.Actual.cellSize;
        timberInstance.transform.position = new Vector3(
            characterPos.x + mapWidth * cellSize * dirShift[dir[curPoint]] / 2f,
            curPos.y,
            spawnPoints[curPoint]
        );
        timberInstance.transform.eulerAngles = new Vector3(curRot.x, angle, curRot.z);
        timberInstance.transform.localScale = new Vector3(
            timberInstance.transform.localScale.x,
            timberInstance.transform.localScale.y,
            cellSize / 2f
        );
        timberInstance.width = rnd.Next(2, 4);
        timberInstance.Run();
        curPoint = (curPoint + 1) % spawnPoints.Length;
    }
}
