﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Train : MonoBehaviour
{
    public Vector3 velocity = Vector3.right;
    public float speed = 22f;

    private Timer lifeTimer;
    private AudioManager audioManager;
    private bool nearPlayer = false;

    public void Run()
    {
        var width = GameConfig.Actual.gridWidth;
        var cellSize = GameConfig.Actual.cellSize;
        var rb = GetComponent<Rigidbody>();
        var curPos = transform.position;
        var newPos = transform.position + velocity * width * cellSize * 2;
        var dist = (newPos - curPos).magnitude;
        var time = dist / speed;
        transform.DOMove(newPos, time, false).OnComplete(Destroy);
        audioManager = AudioManager.Instance;
    }
    
    private void Start()
    {
        lifeTimer = gameObject.AddComponent<Timer>();
    }

    private void Update()
    {
        var characterPos = GameMap.CharacterPosition;
        if ((Mathf.Abs(characterPos.x - transform.position.x) < 60f) &&
            (Mathf.Abs(characterPos.z - transform.position.z) < 8f) &&
            (!nearPlayer))
        {
            nearPlayer = true;
            audioManager.Play("TrainHorn");
        }
    }

    private void Destroy()
    {
        GetComponent<Poolable>().Remove();
    }
}
