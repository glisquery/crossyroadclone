﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainSpawner : MonoBehaviour
{
    private bool updateLocked = true;
    private float[] spawnPoints;
    private int[] dir;
    private Timer timer;
    private int curPoint = 0;
    private float timeoutMin = 0f;
    private float timeoutMax = 0f;
    private float speed = 22f;

    private static Vector3[] dirVec;
    private static float[] dirAngle;
    private static float[] dirShift;
    private static System.Random rnd;
    
    static TrainSpawner()
    {
        dirVec = new Vector3[]
        {
            Vector3.left,
            Vector3.right
        };
        dirAngle = new float[]
        {
            90f,
            -90f
        };
        dirShift = new float[]
        {
            1f,
            -1f
        };
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    public void Unlock()
    {
        updateLocked = false;
    }

    public void SetSpawnPoints(float[] points)
    {
        spawnPoints = points;
        dir = new int[points.Length];
        for (int i = 0; i < points.Length; i++)
        {
            dir[i] = rnd.Next(2);
        }
    }
    
    public void Startup()
    { 
        timeoutMin = GameConfig.Actual.trainSpawnTimeoutMin;
        timeoutMax = GameConfig.Actual.trainSpawnTimeoutMax; 
        curPoint = 0;
        updateLocked = true;
        SetupTimer();
    }

    public void Stop()
    {
        updateLocked = true;
        curPoint = 0;
    }

    private void Update()
    {
        if (updateLocked)
        {
            return;
        }
    }

    private void SetupTimer()
    {
        timer = gameObject.AddComponent<Timer>() as Timer;
        var timeout = Random.Range(timeoutMin, timeoutMax);
        timer.Duration = timeout;
        timer.TimerFinishedEvent.AddListener(SpawnNextTrain);
        timer.Run();
    }

    private void SpawnNextTrain()
    {
        CreateTrain();
        var timeout = Random.Range(timeoutMin, timeoutMax);
        timer.Duration = timeout;
        timer.Run();
    }

    private void CreateTrain()
    {
        GameObject train = null;
        train = Pool.Get("Train");
        var trainInstance = train.GetComponent<Train>();
        trainInstance.velocity = dirVec[dir[curPoint]];
        var curPos = trainInstance.transform.position;
        var curRot = trainInstance.transform.eulerAngles;
        var angle = dirAngle[dir[curPoint]];
        var mapWidth = GameConfig.Actual.gridWidth;
        var cellSize = GameConfig.Actual.cellSize;
        trainInstance.transform.position = new Vector3(
            mapWidth * cellSize * dirShift[dir[curPoint]],
            curPos.y,
            spawnPoints[curPoint]
        );
        trainInstance.transform.eulerAngles = new Vector3(curRot.x, angle, curRot.z);
        trainInstance.speed = speed;
        trainInstance.Run();
        curPoint = (curPoint + 1) % spawnPoints.Length;
    }
}
