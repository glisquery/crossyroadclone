﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FenceSpawner : MonoBehaviour
{
    [SerializeField]
    private Fence treeType1Prefab;
    [SerializeField]
    private Fence treeType2Prefab;
    [SerializeField]
    private Fence treeType3Prefab;
    [SerializeField]
    private Fence stoneType1Prefab;
    [SerializeField]
    private Fence stoneType2Prefab;
    [SerializeField]
    private Coin coinPrefab;
    private bool updateLocked = true;
    private float[] spawnPoints;
    private int[] dir;
    private Timer timer;
    private int curPoint = 0;
    private Fence[] fencePrefabs;
    private string[] fenceNames;
    private List<GameObject> spawnedFences = new List<GameObject>();
    private List<GameObject> coins = new List<GameObject>();

    private static Vector3[] dirVec;
    private static float[] dirAngle;
    private static float[] dirShift;
    private static System.Random rnd;
    
    static FenceSpawner()
    {
        dirVec = new Vector3[]
        {
            Vector3.left,
            Vector3.right
        };
        dirAngle = new float[]
        {
            90f,
            -90f
        };
        dirShift = new float[]
        {
            1f,
            -1f
        };
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    public void Unlock()
    {
        updateLocked = false;
    }

    public void SetSpawnPoints(float[] points)
    {
        spawnPoints = points;
        dir = new int[points.Length];
        for (int i = 0; i < points.Length; i++)
        {
            dir[i] = rnd.Next(2);
        }
    }
    
    private void Start()
    { 
        curPoint = 0;
        updateLocked = true;
        fencePrefabs = new Fence[]
        {
            treeType1Prefab,
            treeType2Prefab,
            treeType3Prefab,
            stoneType1Prefab,
            stoneType2Prefab
        };
        fenceNames = new string[]
        {
            "TreeType1",
            "TreeType2",
            "TreeType3",
            "StoneType1",
            "StoneType2"
        };
        SetupTimer();
    }

    private void Update()
    {
        if (updateLocked)
        {
            return;
        }
        else
        {
            SetupTimer();
        }
    }

    private void SetupTimer()
    {
        SpawnNextFence();
        updateLocked = true;
    }

    private void SpawnNextFence()
    {
        CreateFences();
        CreateForest();
    }

    private void CreateFences()
    {
        if (spawnPoints.Length == 0)
        {
            return;
        }
        var character = Character.Instance;
        var characterPos = GameMap.CharacterPosition;             
        var mapWidth = GameConfig.Actual.gridWidth;
        var cellSize = GameConfig.Actual.cellSize;

        var fenceDist = GameConfig.Actual.fenceDist;
        ArrayList spawnPositionsTmp = new ArrayList();
        float initialGenPos = -cellSize * fenceDist / 2f;
        for (int i = 0; i < mapWidth; i++)
        {
            spawnPositionsTmp.Add(initialGenPos - cellSize * i);
            spawnPositionsTmp.Add(-initialGenPos + cellSize * i);
        }
        float[] spawnPositions = spawnPositionsTmp
            .ToArray(typeof(float))
            as float[];
        for (int i = 0; i < spawnPositions.Length; i++)
        {
            for (int j = 0; j < spawnPoints.Length; j++)
            {
                CreateFence(spawnPositions[i], spawnPoints[j]);
            }
        }
    }

    private void CreateForest()
    {
        if (spawnPoints.Length == 0)
        {
            return;
        }
        var character = Character.Instance;
        var characterPos = GameMap.CharacterPosition;             
        var mapWidth = GameConfig.Actual.gridWidth;
        var cellSize = GameConfig.Actual.cellSize;
        var cntMin = GameConfig.Actual.forestSpawnItemCntMin;
        var cntMax = GameConfig.Actual.forestSpawnItemCntMax;
        var cnt = rnd.Next(cntMin, cntMax + 1);
        var fenceDist = GameConfig.Actual.fenceDist;
        ArrayList spawnPositionsTmp = new ArrayList();
        float initialGenPos = -cellSize;
        for (int i = 0; i < cntMax; i++)
        {
            spawnPositionsTmp.Add(initialGenPos - cellSize * i);
            spawnPositionsTmp.Add(-initialGenPos + cellSize * i);
        }
        float[] spawnPositions = spawnPositionsTmp
            .ToArray(typeof(float))
            as float[];
        float[] shuffledPositions = (spawnPositions)
            .OrderBy(x => rnd.Next()).ToArray();     
        float[] shuffledPoints = (spawnPoints)
            .OrderBy(x => rnd.Next()).ToArray();
        for (int i = 0; i < cnt; i++)
        {
            var j = i % spawnPositions.Length;
            var k = i % spawnPoints.Length;
            var nxt = rnd.Next(0, 6);
            if (nxt == 3)
            {
                CreateCoin(spawnPositions[j], spawnPoints[k]);
            }
            else
            {
                CreateFence(spawnPositions[j], spawnPoints[k]);
            }
        }
    }
    
    private void CreateFence(float posX, float posZ)
    {
        var next = rnd.Next(0, fencePrefabs.Length);
        GameObject fence = Pool.Get(fenceNames[next]);
        spawnedFences.Add(fence);
        var curPos = fence.transform.position;
        var curRot = fence.transform.eulerAngles;
        var angle = dirAngle[dir[curPoint]];
        fence.transform.position = new Vector3(
            posX,
            curPos.y,
            posZ
        );
        fence.transform.eulerAngles = new Vector3(curRot.x, angle, curRot.z);
        curPoint = (curPoint + 1) % spawnPoints.Length;
    }

    private void CreateCoin(float posX, float posZ)
    {
        GameObject coin = null;
        coin = Pool.Get("Coin", gameObject);
        coins.Add(coin);
        var curPos = coin.transform.position;
        var curRot = coin.transform.eulerAngles;
        var angle = dirAngle[dir[curPoint]];
        coin.transform.position = new Vector3(
            posX,
            curPos.y,
            posZ
        );
        coin.transform.eulerAngles = new Vector3(curRot.x, angle, curRot.z);
        curPoint = (curPoint + 1) % spawnPoints.Length;
    }

    private void OnDestroy()
    {
        foreach (var fence in spawnedFences)
        {
            if (fence)
            {
                fence.GetComponent<Poolable>().Remove();
            }
        }
        foreach (var coin in coins)
        {
            if (coin)
            {
                coin.GetComponent<Poolable>().Remove(gameObject);
            }
        }
        coins.Clear();
    }
}
