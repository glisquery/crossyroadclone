﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootpathLayer : MapLayer
{
    [SerializeField]
    private Poolable layerTerrain;
    [SerializeField]
    private FenceSpawner fenceSpawner; 
    private FootpathLayerTerrain terrain;
    private FenceSpawner spawner;
    private CoinSpawner coinSpawnerInstance;
    private int height;
    private float cellSize;
    
    private static System.Random rnd;
    
    static FootpathLayer()
    {
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    public void Startup()
    {
        cellSize = GameConfig.Actual.cellSize;
        CreateLayer();
    }

    private void Update()
    {
        var characterInstance = Character.Instance;

        if (!characterInstance)
        {
            return;
        }

        if (!terrain)
        {
            return;
        }

        var characterPos = characterInstance.transform.position.z;
        var mapHeight = GameConfig.Actual.gridHeight;

        if (characterPos -
            (terrain.transform.position.z)
            > mapHeight)
        {
            GetComponent<Poolable>().Remove();
            terrain.GetComponent<Poolable>().Remove();
            Destroy(spawner.gameObject);
        }
    }

    private void CreateLayer()
    {
        CreateTerrain();
        CreateSpawner();
    }

    private void CreateTerrain()
    {
        terrain = Pool.Get<FootpathLayerTerrain>(layerTerrain);
        var cellSize = GameConfig.Actual.cellSize;
        var width = GameConfig.Actual.gridWidth;
        var minTracks = GameConfig.Actual.footpathNumTracksMin;
        var maxTracks = GameConfig.Actual.footpathNumTracksMax;
        height = rnd.Next(minTracks, maxTracks + 1);
        var terrainTransform = terrain.transform;
        var curScale = terrainTransform.localScale;
        var newScale = new Vector3(width, curScale.y, height * cellSize);
        terrainTransform.localScale = newScale; 
        var oldPos = terrainTransform.position;
        float edge = GameMap.MapEdge;
        var newPos = oldPos + new Vector3(0, 0f, edge + height - cellSize);
        terrainTransform.position = newPos;  
        GameMap.MapEdge = newPos.z + height * cellSize + cellSize; 
    }

    private void CreateSpawner()
    {
        spawner = Instantiate(fenceSpawner);
        var cellSize = GameConfig.Actual.cellSize;
        var terrainPos = terrain.transform.position.z;
        var terrainBegin = terrainPos - terrain.transform.localScale.z;
        float[] positions = new float[height];
        ArrayList fencePositions = new ArrayList();
        terrainBegin += cellSize;
        for (int i = 0; i < height; i++)
        {
            positions[i] = terrainBegin + cellSize * 2 * i;
            fencePositions.Add(positions[i]);
        }
        spawner.SetSpawnPoints(fencePositions.ToArray(typeof(float)) as float[]);
        spawner.Unlock(); 
    }
}
