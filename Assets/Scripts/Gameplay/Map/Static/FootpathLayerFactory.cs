﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FootpathLayerFactory : MonoBehaviour
{
    private static FootpathLayerFactory instance = null;
    private static readonly object padlock = new object();

    [SerializeField]
    private Poolable footpathLayer;
    private UnityEvent layerFactoryReadyEvent = new UnityEvent();
    private int width;
    private int height;
    private float cellSize;

    private static System.Random rnd;

    public static FootpathLayerFactory Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    }
    
    static FootpathLayerFactory()
    {
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    public UnityEvent LayerFactoryReadyEvent
    {
        get
        {
            return layerFactoryReadyEvent;
        }
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        var gameMap = GameMap.Instance;
        gameMap.FootpathLayerSpawnRequestEvent.AddListener(CreateLayer);
        layerFactoryReadyEvent.Invoke();
    }
    
    private void CreateLayer()
    { 
        Pool.Get<FootpathLayer>(footpathLayer).Startup();
    }
}
