﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootpathLayerTerrain : MonoBehaviour
{
    private Vector3 initialScale;
    private Vector3 initialPosition;
    private Quaternion initialRotation;

    private void Start()
    {
        GetComponent<Poolable>().OnCreate.AddListener(Reset);
        initialScale = transform.localScale;
        initialPosition = transform.position;
        initialRotation = transform.rotation;
    }

    private void Reset()
    {
        transform.position = new Vector3(
            0f,
            initialPosition.y,
            0f
        );
        transform.rotation = new Quaternion();
        transform.localScale =  initialScale;
    }
}
