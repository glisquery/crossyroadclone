﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HighwayLayerFactory : MonoBehaviour
{
    private static HighwayLayerFactory instance;
    private static readonly object padlock = new object();

    [SerializeField]
    private Poolable highwayLayer;

    private UnityEvent layerFactoryReadyEvent = new UnityEvent();
    private int width;
    private int height;
    private float cellSize;

    private static System.Random rnd;

    public static HighwayLayerFactory Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    }

    public UnityEvent LayerFactoryReadyEvent
    {
        get
        {
            return layerFactoryReadyEvent;
        }
    } 

    static HighwayLayerFactory()
    {
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        var gameMap = GameMap.Instance;
        gameMap.HighwayLayerSpawnRequestEvent.AddListener(CreateLayer);
        layerFactoryReadyEvent.Invoke();
    }

    private void CreateLayer()
    {
        Pool.Get<HighwayLayer>(highwayLayer).Startup();
    }
}
