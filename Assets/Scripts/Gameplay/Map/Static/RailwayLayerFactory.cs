﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RailwayLayerFactory : MonoBehaviour
{
    private static RailwayLayerFactory instance = null;
    private static readonly object padlock = new object();

    [SerializeField]
    private Poolable railwayLayer;  
    private UnityEvent layerFactoryReadyEvent = new UnityEvent();
    private int width;
    private int height;
    private float cellSize;

    private static System.Random rnd;

    public static RailwayLayerFactory Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    }

    public UnityEvent LayerFactoryReadyEvent
    {
        get
        {
            return layerFactoryReadyEvent;
        }
    } 

    static RailwayLayerFactory()
    {
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        var gameMap = GameMap.Instance;
        gameMap.RailwayLayerSpawnRequestEvent.AddListener(CreateLayer);
        layerFactoryReadyEvent.Invoke();
    }

    private void CreateLayer()
    {
        Pool.Get<RailwayLayer>(railwayLayer).Startup();
    }
}
