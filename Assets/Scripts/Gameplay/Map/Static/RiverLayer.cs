﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverLayer : MapLayer
{
    [SerializeField]
    private Poolable layerTerrain;
    [SerializeField]
    private TimberSpawner timberSpawner;
    [SerializeField]
    private LilySpawner lilySpawnerPrefab;
    
    private RiverLayerTerrain terrain;
    private TimberSpawner spawner;
    private LilySpawner lilySpawner;
    private int height;
    private float cellSize;

    private static System.Random rnd;
    
    static RiverLayer()
    {
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    public void Startup()
    {
        cellSize = GameConfig.Actual.cellSize;
        CreateLayer();
    }

    private void Update()
    {
        var characterInstance = Character.Instance;
        if (!characterInstance)
        {
            return;
        }
        var characterPos = characterInstance.transform.position.z;
        var mapHeight = GameConfig.Actual.gridHeight;
        if (!terrain)
        {
            return;
        }

        if (characterPos -
            (terrain.transform.position.z)
            > mapHeight)
        {
            GetComponent<Poolable>().Remove();
            terrain.GetComponent<Poolable>().Remove();
            Destroy(spawner.gameObject);
            Destroy(lilySpawner.gameObject);
        }
    }

    private void CreateLayer()
    {
        CreateTerrain();
        CreateSpawner();
    }

    private void CreateTerrain()
    {
        terrain = Pool.Get<RiverLayerTerrain>(layerTerrain);
        var cellSize = GameConfig.Actual.cellSize;
        var width = GameConfig.Actual.gridWidth;
        var minTracks = GameConfig.Actual.riverNumTracksMin;
        var maxTracks = GameConfig.Actual.riverNumTracksMax;
        height = rnd.Next(minTracks, maxTracks + 1);
        var terrainTransform = terrain.transform;
        var curScale = terrainTransform.localScale;
        var newScale = new Vector3(width, curScale.y, height * cellSize);
        var oldPos = terrainTransform.position;
        float edge = GameMap.MapEdge;
        var newPos = oldPos + new Vector3(0f, 0f, edge + height - cellSize);
        terrainTransform.position = newPos; 
        terrain
            .gameObject
            .GetComponent<BoxCollider>()
            .size = new Vector3(
                newScale.x,
                newScale.y,
                cellSize * height + cellSize * (height % 2)
        );
        terrain
            .gameObject
            .GetComponent<BoxCollider>()
            .center = new Vector3(
                0,
                -90,
                0
        );
        terrainTransform.localScale = newScale; 
        GameMap.MapEdge = newPos.z + height * cellSize + cellSize; 
    }

    private void CreateSpawner()
    {
        spawner = Instantiate(timberSpawner);
        lilySpawner = Instantiate(lilySpawnerPrefab);
        var cellSize = GameConfig.Actual.cellSize;
        var terrainPos = terrain.transform.position.z;
        var terrainBegin = terrainPos - terrain.transform.localScale.z;
        float[] positions = new float[height];
        ArrayList lilyPositions = new ArrayList();
        ArrayList timberPositions = new ArrayList();
        terrainBegin += cellSize;
        for (int i = 0; i < height; i++)
        {
            positions[i] = terrainBegin + cellSize * 2 * i;
            var next = rnd.Next(2);
            if (next == 0)
            {
                lilyPositions.Add(positions[i]);
            }
            else
            {
                timberPositions.Add(positions[i]);
            }
        }
        spawner.SetSpawnPoints(timberPositions.ToArray(typeof(float)) as float[]);
        lilySpawner.SetSpawnPoints(lilyPositions.ToArray(typeof(float)) as float[]);
        spawner.Unlock();
        lilySpawner.Unlock();
    }
}
