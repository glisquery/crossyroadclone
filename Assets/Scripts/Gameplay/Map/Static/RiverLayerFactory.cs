﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RiverLayerFactory : MonoBehaviour
{
    private static RiverLayerFactory instance = null;
    private static readonly object padlock = new object();

    [SerializeField]
    private Poolable riverLayer;
    private UnityEvent layerFactoryReadyEvent = new UnityEvent();   
    private int width;
    private int height;
    private float cellSize;

    private static System.Random rnd;

    public static RiverLayerFactory Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    }

    public UnityEvent LayerFactoryReadyEvent
    {
        get
        {
            return layerFactoryReadyEvent;
        }
    } 

    static RiverLayerFactory()
    {
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        var gameMap = GameMap.Instance;
        gameMap.RiverLayerSpawnRequestEvent.AddListener(CreateLayer);
        layerFactoryReadyEvent.Invoke();
    }

    private void Update()
    { 
    }

    private void CreateLayer()
    {
        Pool.Get<RiverLayer>(riverLayer).Startup();
    }
}
