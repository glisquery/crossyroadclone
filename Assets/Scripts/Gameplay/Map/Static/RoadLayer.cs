﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoadLayer : MapLayer
{
    [SerializeField]
    private Poolable layerTerrain;
    [SerializeField]
    private Poolable carSpawner;
    [SerializeField]
    private Coin coinPrefab;
    private RoadLayerTerrain terrain;
    private CarSpawner spawner;
    private int height;
    private List<GameObject> coins = new List<GameObject>();
    private static int mapHeight;
    private static float cellSize;
    private static int width;
    private static int minTracks; 
    private static int maxTracks;
    private static int fenceDist;
    private static System.Random rnd;
    
    static RoadLayer()
    {
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    private void Awake()
    {
        GameConfig.Init();
    }

    public void Startup()
    {
        CreateLayer();
    }

    private void Update()
    {
        var characterInstance = Character.Instance;
        if (!characterInstance)
        {
            return;
        }
        var characterPos = characterInstance.transform.position.z;
        var mapHeight = GameConfig.Actual.gridHeight;

        if (!terrain)
        {
            return;
        }


        if (characterPos -
            (terrain.transform.position.z)
            > mapHeight)
        {
            GetComponent<Poolable>().Remove();
            terrain.GetComponent<Poolable>().Remove();
            spawner.GetComponent<Poolable>().Remove();
            spawner.Stop();
            foreach (var coin in coins)
            {
                coin.GetComponent<Poolable>().Remove(gameObject);
            }
            coins.Clear();
        }
    }

    private void CreateLayer()
    {
        CreateTerrain();
        CreateSpawner();
    }

    private void CreateTerrain()
    {
        terrain = Pool.Get<RoadLayerTerrain>(layerTerrain);

        var cellSize = GameConfig.Actual.cellSize;
        var width = GameConfig.Actual.gridWidth;
        var minTracks = GameConfig.Actual.roadNumTracksMin;
        var maxTracks = GameConfig.Actual.roadNumTracksMax;
        height = rnd.Next(minTracks, maxTracks + 1);
        var terrainTransform = terrain.transform;
        var curScale = terrainTransform.localScale;
        var newScale = new Vector3(width, curScale.y, height * cellSize);
        terrainTransform.localScale = newScale; 
        var oldPos = terrainTransform.position;
        float edge = GameMap.MapEdge;
        var newPos = oldPos + new Vector3(0f, 0f, edge + height - cellSize);
        terrainTransform.position = newPos; 
        GameMap.MapEdge = newPos.z + height * cellSize + cellSize; 
    }

    private void CreateSpawner()
    {
        spawner = Pool.Get<CarSpawner>(carSpawner);
        spawner.Startup();
        var cellSize = GameConfig.Actual.cellSize;
        var terrainPos = terrain.transform.position.z;
        var terrainBegin = terrainPos - terrain.transform.localScale.z;
        float[] positions = new float[height];
        terrainBegin += cellSize;
        for (int i = 0; i < height; i++)
        {
            positions[i] = terrainBegin + cellSize * 2 * i; 
        }
        spawner.SetSpawnPoints(positions);
        spawner.Unlock();

        float[] spawnPoints = positions;
        var cntMin = 0;
        var cntMax = 5;
        var cnt = rnd.Next(cntMin, cntMax + 1);
        var fenceDist = GameConfig.Actual.fenceDist;
        ArrayList spawnPositionsTmp = new ArrayList();
        float initialGenPos = -cellSize;
        for (int i = 0; i < cntMax; i++)
        {
            spawnPositionsTmp.Add(initialGenPos - cellSize * i);
            spawnPositionsTmp.Add(-initialGenPos + cellSize * i);
        }
        float[] spawnPositions = spawnPositionsTmp
            .ToArray(typeof(float))
            as float[];
        float[] shuffledPositions = (spawnPositions)
            .OrderBy(x => rnd.Next()).ToArray();     
        float[] shuffledPoints = (spawnPoints)
            .OrderBy(x => rnd.Next()).ToArray();
        for (int i = 0; i < cnt; i++)
        {
            var j = i % spawnPositions.Length;
            var k = i % spawnPoints.Length;
            var nxt = rnd.Next(0, 6);
            if (nxt == 3)
            {
                CreateCoin(spawnPositions[j], spawnPoints[k]);
            }
        }
    }


    private void CreateCoin(float posX, float posZ)
    {
        GameObject coin = null;
        coin = Pool.Get("Coin", gameObject);
        coins.Add(coin);
        var curPos = coin.transform.position;
        var curRot = coin.transform.eulerAngles;
        coin.transform.position = new Vector3(
            posX,
            curPos.y,
            posZ
        );
    }
}
