﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RoadLayerFactory : MonoBehaviour
{
    private static RoadLayerFactory instance;
    private static readonly object padlock = new object();
    
    [SerializeField]
    public Poolable roadLayer;
    private UnityEvent layerFactoryReadyEvent = new UnityEvent();
    private int width;
    private int height;
    private float cellSize;

    private static System.Random rnd;

    public static RoadLayerFactory Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    }

    public UnityEvent LayerFactoryReadyEvent
    {
        get
        {
            return layerFactoryReadyEvent;
        }
    } 

    static RoadLayerFactory()
    {
        rnd = new System.Random((int)System.DateTime.Now.Ticks & 0x0000FFFF);
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        var gameMap = GameMap.Instance;
        gameMap.RoadLayerSpawnRequestEvent.AddListener(CreateLayer);
        layerFactoryReadyEvent.Invoke();
    }

    private void CreateLayer()
    {
        Pool.Get<RoadLayer>(roadLayer).Startup();
    }
}
