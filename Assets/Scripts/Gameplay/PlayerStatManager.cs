﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerStatManager : MonoBehaviour
{
    private static PlayerStatManager instance = null; 
    private static readonly object padlock = new object();
    private static UnityEvent initializedEvent = new UnityEvent();
    
    private UnityEvent coinPickupEvent = new UnityEvent();
    private UnityEvent scoreChangeEvent = new UnityEvent();
    private Character characterInstance = null;
    private int numCoins = 0;
    private int score = 0;
    private int record = 0;

    public static PlayerStatManager Instance
    {
        get
        {
            lock(padlock)
            {
                return instance;
            }
        }
    }

    public static UnityEvent InitializedEvent
    {
        get
        {
            return initializedEvent;
        }
    }

    public UnityEvent CoinPickupEvent
    {
        get
        {
            return coinPickupEvent;
        }
    }

    public UnityEvent ScoreChangeEvent
    {
        get
        {
            return scoreChangeEvent;
        }
    }

    public int Score
    {
        get
        {
            return score;
        }
    }

    public int Coins
    {
        get
        {
            return numCoins;
        }
    }

    public int Record
    {
        get
        {
            return record;
        }
    }

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        numCoins = GameConfig.Actual.coins;
        record = GameConfig.Actual.record;
        initializedEvent.Invoke();
    }

    private void Update()
    {
        if (!characterInstance)
        {
            characterInstance = Character.Instance;
            if (characterInstance)
            {
                characterInstance.CoinPickupEvent.AddListener(OnCoinPickup);
                characterInstance.MoveEvent.AddListener(OnMove);
            }
        }
    }

    private void OnCoinPickup()
    {
        numCoins++;
        GameConfig.Config.Set("coins", numCoins.ToString());
        GameConfig.Sync();
        GameConfig.Save();
        coinPickupEvent.Invoke();
    }

    private void OnMove()
    {
        var pos = GameMap.CharacterPosition;
        var cell = GameMap.GetCellY(pos);
        var prevRecord = record;
        score = System.Math.Max(score, cell / 2);
        record = System.Math.Max(record, score);

        if (record != prevRecord)
        {
            GameConfig.Config.Set("record", record.ToString());
            GameConfig.Sync();
            GameConfig.Save();
        }

        scoreChangeEvent.Invoke();
    }
}
