﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using DG.Tweening;

public class InputManager : MonoBehaviour
{
    private static bool disabled = false;
    
    private delegate void MovementProc();
    private Vector3 newPos = new Vector3(0f, 0.35f, 0f);
    private Timer moveTimer;
    private float moveTime = 0.2f;
    private RuntimePlatform platform;
    private Vector2 startPos = Vector2.zero;
    private Vector2 dir = Vector2.zero;
    private bool dirChosen = false;
    private Queue<MovementProc> inputHandlers = new Queue<MovementProc>();

    public static void DisableInput()
    {
        disabled = true;
    }

    public static void EnableInput()
    {
        disabled = false;
    }
 
    private static bool OverUI()
    {
        PointerEventData eventDataCurrentPosition =
            new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position =
            new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        for (int i = 0; i < results.Count; i++)
        {
            if (results[i].gameObject.layer == 5)
            {
                return true;
            }
        }

        return false;
    }
    
    private void Start()
    {
        platform = Application.platform;
        SetupComponents();
    }

    private void Update()
    { 
        if ((disabled) || (OverUI()))
        {
            return;
        }

        switch (platform)
        {
            case RuntimePlatform.WindowsPlayer:
                HandleMovementPC();
                break;
            case RuntimePlatform.WindowsEditor:
                HandleMovementPC();
                break;
            case RuntimePlatform.Android:
                HandleMovementAndroid();
                break;
            default:
                HandleMovementPC();
                break;
        }

        ProcessEnqueuedInput();

        var cubeInst = Character.Instance;
        var cameraInst = Character.Instance;
        var cellSize = GameConfig.Actual.cellSize;
        var mapWidth = GameConfig.Actual.gridWidth;
        if ((cameraInst) && (cubeInst))
        {
            var characterPos = cubeInst.transform.position.x;
            var newCameraPos = cubeInst.transform.position;
            newCameraPos.y = cameraInst.transform.position.y;
            newCameraPos += new Vector3(5f, 0f, 8f);
        } 
    }

    private void ProcessEnqueuedInput()
    {
        if (moveTimer.Running)
        {
            return;
        }

        if (inputHandlers.Count > 0)
        {
            inputHandlers.Dequeue()();
        }
    }

    private void HandleMovementPC()
    {
        var charInst = Character.Instance;
        if (!charInst)
        {
            return;
        }

        MovementProc nextProc = null;
        if (Input.GetKeyDown("w"))
        {
            nextProc = MoveForward;
        }
        if (Input.GetKeyDown("s"))
        {
            nextProc = MoveBack;
        }
        if (Input.GetKeyDown("a"))
        {
            nextProc = MoveLeft;
        }
        if (Input.GetKeyDown("d"))
        {
            nextProc = MoveRight;
        }
        if ((nextProc != null) && (inputHandlers.Count < 3))
        {
            inputHandlers.Enqueue(nextProc);
        }

        HandleMouseInput();
    }

    private void HandleMouseInput()
    {    
        var mousePos = new Vector2(
            Input.mousePosition.x,
            Input.mousePosition.y
        );
        if (Input.GetMouseButtonDown(0))
        { 
            startPos = mousePos;
            dirChosen = false;
        }
        else if (Input.GetMouseButton(0))
        {
            dir = mousePos - startPos;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            dirChosen = true;
        }
        if (dirChosen)
        {
            HandleMouseMove();
            dirChosen = false;
        } 
    }

    private void HandleMouseMove()
    {
        Vector2 mouseDir = new Vector2(dir.x, dir.y);

        if (mouseDir.magnitude < 25f)
        {
            inputHandlers.Enqueue(MoveForward);
            return;
        }

        mouseDir = mouseDir.normalized;
        Vector2[] axes = new Vector2[]
        {
            Vector2.up,
            Vector2.down,
            Vector2.left,
            Vector2.right
        };
        MovementProc[] handlers = new MovementProc[]
        {
            MoveForward,
            MoveBack,
            MoveLeft,
            MoveRight
        };
        var minPos = 0;
        for (var i = 0; i < axes.Length; i++)
        {
            var curLen = (dir - axes[i]).magnitude;
            if (curLen < (dir - axes[minPos]).magnitude)
            {
                minPos = i;
            }
        }
        if (inputHandlers.Count < 3)
        {
            inputHandlers.Enqueue(handlers[minPos]);
        }
    }
    
    private void HandleMovementAndroid()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startPos = touch.position;
                    dirChosen = false;
                    break;
                case TouchPhase.Moved:
                    dir = touch.position - startPos;
                    break;
                case TouchPhase.Ended:
                    dir = touch.position - startPos;
                    dirChosen = true;
                    break;
            }
        }
        if (dirChosen)
        {
            HandleMouseMove();
            dirChosen = false;
        }
    }       

    private void MoveForward()
    {
        var character = Character.Instance;
        character.MoveForward();
        SetMovementTimer();
    }

    private void MoveBack()
    {
        var character = Character.Instance;
        character.MoveBack();
        SetMovementTimer();
     
    }

    private void MoveLeft()
    {
        var character = Character.Instance;
        character.MoveLeft();
        SetMovementTimer();
    }

    private void MoveRight()
    {
        var character = Character.Instance;
        character.MoveRight();
        SetMovementTimer();
    }
   
    private void SetMovementTimer()
    {
        moveTimer.Duration = moveTime - 0.001f;
        moveTimer.Run();
    }

    private void SetupComponents()
    {
        moveTimer = gameObject.AddComponent<Timer>() as Timer;
    }
}
