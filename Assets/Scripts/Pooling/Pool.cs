﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [System.Serializable]
    public class PoolingInfo
    {
        public Poolable prefab;
        public PoolInstance pool;
    }

    private static Dictionary<string, PoolingInfo> poolingItems;
    private static Pool instance = null;

    [SerializeField]
    private List<PoolingInfo> initial;

    public static GameObject Get(string name, GameObject parent = null)
    {
        if (poolingItems.ContainsKey(name))
        {
            return poolingItems[name].pool.Get(parent);
        }

        return null;
    }

    public static GameObject Get(Poolable prefab, GameObject parent = null)
    {
        if (!poolingItems.ContainsKey(prefab.name))
        {
            instance.CreateNewPool(prefab);
        }

        return poolingItems[prefab.name].pool.Get(parent);
    }

    public static T Get<T>(Poolable prefab, GameObject parent = null)
    {
        if (!poolingItems.ContainsKey(prefab.name))
        {
            instance.CreateNewPool(prefab);
        }

        return poolingItems[prefab.name].pool.Get(parent).GetComponent<T>();
    }

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        if (instance)
        {
            return;
        }

        instance = this;
        poolingItems = new Dictionary<string, PoolingInfo>();
        foreach (var poolingItem in initial)
        {
            poolingItem.pool = new PoolInstance();
            poolingItem.pool.Init(poolingItem.prefab);
            poolingItems.Add(poolingItem.prefab.name, poolingItem);
        }
    }

    private void CreateNewPool(Poolable prefab)
    {
        var newPoolingInfo = new PoolingInfo();
        newPoolingInfo.prefab = prefab;
        newPoolingInfo.pool = new PoolInstance();
        newPoolingInfo.pool.Init(prefab);
        poolingItems.Add(prefab.name, newPoolingInfo);
    }
}
