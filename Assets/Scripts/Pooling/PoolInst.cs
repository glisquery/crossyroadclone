﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolInstance
{
    private List<GameObject> objects = new List<GameObject>();
    private Poolable prefab;
    
    public void Init(Poolable prefab)
    {
        this.prefab = prefab; 
    }

    public GameObject Get()
    {
        foreach (var obj in objects)
        {
            if (!obj.activeInHierarchy)
            {
                return obj.GetComponent<Poolable>().Create();
            }
        }
        Add(prefab);

        return objects[objects.Count - 1].GetComponent<Poolable>().Create();
    }

    public GameObject Get(GameObject parent)
    {
        foreach (var obj in objects)
        {
            if (!obj.activeInHierarchy)
            {
                return obj.GetComponent<Poolable>().Create(parent);
            }
        }
        Add(prefab);

        return (
            objects[objects.Count - 1]
                .GetComponent<Poolable>()
                .Create(parent)
        );
    }

    private void Add(Poolable prefab)
    {
        GameObject obj = GameObject.Instantiate(prefab.gameObject);
        obj.SetActive(false);
        objects.Add(obj);
    }
}
