﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Poolable : MonoBehaviour
{
    private UnityEvent onCreate = new UnityEvent();
    private GameObject parent = null;

    public UnityEvent OnCreate
    {
        get
        {
            return onCreate;
        }
    }
    
    public GameObject Create()
    {
        gameObject.SetActive(true);
        onCreate.Invoke();

        return gameObject;
    }

    public GameObject Create(GameObject parent)
    {
        this.parent = parent;
        gameObject.SetActive(true);
        onCreate.Invoke();

        return gameObject;
    }
    
    public void Remove()
    {
        gameObject.SetActive(false);
    }

    public void Remove(GameObject parent)
    {
        if (parent == this.parent)
        {
            gameObject.SetActive(false);
        }
    }
}
