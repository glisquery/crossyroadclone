﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using TMPro;

public class HUDManager : MonoBehaviour
{
    [SerializeField]
    private RectTransform pausePanelTransform;
    [SerializeField]
    private RectTransform gameOverPanelTransform;
    [SerializeField]
    private RectTransform pauseButtonTransform;
    [SerializeField]
    private RectTransform muteButtonTransform;
    [SerializeField]
    private RectTransform unmuteButtonTransform;
    [SerializeField]
    private TextMeshProUGUI tmproScore;
    [SerializeField]
    private TextMeshProUGUI tmproCoins;
    [SerializeField]
    private TextMeshProUGUI tmproFinalScore;
    
    private bool gameIsOver = false;
    private GameStateManager gameStateManager = null;
    private PlayerStatManager playerStatManager = null;
    private Timer timer = null;
    
    private static Vector3 oneVec = new Vector3(1f, 1f, 1f);

    public void OnApplicationFocus(bool hasFocus)
    {
        if (!hasFocus)
        {
            OnClickPause();
        }
    }

    public void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            OnClickPause();
        }
    }
    
    public void OnClickResume()
    {
        Time.timeScale = 1f;
        GameplayCamera.Locked = false;
        Timer.active = true;
        timer.Duration = 0.5f;
        timer.TimerFinishedEvent.AddListener(InputManager.EnableInput);
        timer.Run();
        HidePanel(pausePanelTransform);
        ShowPanel(pauseButtonTransform);
        UpdateMuteButtonState();
    }

    public void OnClickExit()
    {
        Timer.active = true;
        Time.timeScale = 1f;
        GameplayCamera.Locked = false;
        InputManager.EnableInput();
        SceneManager.LoadScene("MainMenuScene");
    }

    public void OnClickPause()
    {
        InputManager.DisableInput();
        Time.timeScale = 0f;
        Timer.active = false;
        GameplayCamera.Locked = true;
        ShowPanel(pausePanelTransform);
        HidePanel(pauseButtonTransform);
        UpdateMuteButtonState();
    }

    public void OnClickRestart()
    {
        InputManager.EnableInput();
        Timer.active = true;
        Time.timeScale = 1f;
        GameplayCamera.Locked = false;
        SceneManager.LoadScene("GameplayScene");
    }

    public void OnClickMute()
    {
        GameConfig.Init();
        var mute = !GameConfig.Actual.mute;
        GameConfig.Config.Set("mute", mute.ToString());
        GameConfig.Sync();
        GameConfig.Save();
        UpdateMuteButtonState();
        AudioManager.Instance.UpdateState();
    }

    public void OnCoinPickup()
    {
        var playerStatManager = PlayerStatManager.Instance;
        tmproCoins.text = playerStatManager.Coins.ToString();
    }

    public void OnMove()
    {
        var score = playerStatManager.Score.ToString();
        var record = playerStatManager.Record.ToString();
        tmproScore.text = (
            "Score: " + score.ToString() + "\n" +
            "Record: " + record.ToString()
        );
    }
    
    private void Start()
    {
        timer = gameObject.AddComponent<Timer>();
        SetSubscriptions();
        SetInitialPanelsState();
    }

    private void Update()
    { 
        if (!gameStateManager)
        {
            gameStateManager = GameStateManager.Instance;
            if (gameStateManager)
            {
                gameStateManager.GameOverEvent.AddListener(OnGameOver);
            }
        }
    }

    private void SetInitialData()
    {
        playerStatManager = PlayerStatManager.Instance;
        playerStatManager.CoinPickupEvent.AddListener(OnCoinPickup);
        playerStatManager.ScoreChangeEvent.AddListener(OnMove);
        tmproCoins.text = playerStatManager.Coins.ToString();
        var record = playerStatManager.Record;
        tmproScore.text = "Score: 0\nRecord: " + record.ToString();
    }

    private void OnGameOver()
    {
        if (!gameIsOver)
        {
            InputManager.DisableInput();
            ShowPanel(gameOverPanelTransform);
            UpdateMuteButtonState();
            tmproFinalScore.text = tmproScore.text;
            gameIsOver = true;
        }
    }

    private void SetSubscriptions()
    {
        PlayerStatManager.InitializedEvent.AddListener(SetInitialData);
        playerStatManager = PlayerStatManager.Instance;
        if (playerStatManager)
        {
            playerStatManager.CoinPickupEvent.AddListener(OnCoinPickup);
            playerStatManager.ScoreChangeEvent.AddListener(OnMove);
        }
    }
    
    private void UpdateMuteButtonState()
    {
        HidePanel(muteButtonTransform);
        HidePanel(unmuteButtonTransform);
        GameConfig.Init();
        if (GameConfig.Actual.mute)
        {
            ShowPanel(unmuteButtonTransform);
        }
        else
        {
            ShowPanel(muteButtonTransform);
        }
    }
    
    private void SetInitialPanelsState()
    {
        HidePanel(pausePanelTransform);
        HidePanel(gameOverPanelTransform);
        UpdateMuteButtonState();
    }

    private void HidePanel(RectTransform rectTransform)
    {
        rectTransform.localScale = Vector3.zero;
    }

    private void ShowPanel(RectTransform rectTransform)
    {
        rectTransform.localScale = Vector3.one;
    }
}
