﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingSplashManager : MonoBehaviour
{
    private Timer timer;
    
    private void Start()
    {
        timer = gameObject.AddComponent<Timer>();
        timer.Duration = 5.0f;
        timer.TimerFinishedEvent.AddListener(LoadGameplay);
        timer.Run();
    }

    private void LoadGameplay()
    {
        SceneManager.LoadScene("GameplayScene");
    }
}
