﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField]
    private RectTransform muteButtonTransform;
    [SerializeField]
    private RectTransform unmuteButtonTransform;
    [SerializeField]
    private AudioManager audioManagerPrefab; 
    [SerializeField]
    private TextMeshProUGUI tmproCoins;
    
    public void OnClickPlay()
    {
        AudioManager.Instance.Play("Click");
        SceneManager.LoadScene("LoadingSplashScene");
    }
    
    public void OnClickMute()
    {
        AudioManager.Instance.Play("Click");
        var mute = !GameConfig.Actual.mute;
        GameConfig.Config.Set("mute", mute.ToString());
        GameConfig.Sync();
        GameConfig.Save();
        UpdateMuteButtonState();
        AudioManager.Instance.UpdateState();
    }

    private void UpdateMuteButtonState()
    {
        HidePanel(muteButtonTransform);
        HidePanel(unmuteButtonTransform);
        GameConfig.Init();
        if (GameConfig.Actual.mute)
        {
            ShowPanel(unmuteButtonTransform);
        }
        else
        {
            ShowPanel(muteButtonTransform);
        }
    }

    private void HidePanel(RectTransform rectTransform)
    {
        rectTransform.localScale = Vector3.zero;
    }

    private void ShowPanel(RectTransform rectTransform)
    {
        rectTransform.localScale = Vector3.one;
    }

    private void Start()
    {
        GameConfig.Init();
        Instantiate(audioManagerPrefab);
        AdjustElements();
    }

    private void AdjustElements()
    {
        UpdateCoinCounter();
        UpdateMuteButtonState();
    }

    private void UpdateCoinCounter()
    {
        tmproCoins.text = GameConfig.Actual.coins.ToString(); 
    }
}
