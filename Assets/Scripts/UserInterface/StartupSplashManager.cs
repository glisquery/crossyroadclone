﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartupSplashManager : MonoBehaviour
{
    private Timer timer;
    
    private void Start()
    {
        timer = gameObject.AddComponent<Timer>();
        timer.Duration = 8.0f;
        timer.TimerFinishedEvent.AddListener(LoadMainMenu);
        timer.Run();
    }

    private void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
