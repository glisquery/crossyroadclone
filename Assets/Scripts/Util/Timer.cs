﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    public static bool active = true;
  
    private UnityEvent timerChangedEvent = new UnityEvent();
    private UnityEvent timerFinishedEvent = new UnityEvent();
    private float totalSeconds = 0;
	private float elapsedSeconds = 0;
	private bool running = false;
	private bool started = false;

    public UnityEvent TimerChangedEvent
    {
        get
        {
            return timerChangedEvent;
        }
    }

    public UnityEvent TimerFinishedEvent
    {
        get
        {
            return timerFinishedEvent;
        }
    }

    public float Duration
    {
		set
        {
			if (!running)
            {
				totalSeconds = value;
			}
		}
	}
	
    public bool Finished
    {
		get { return started && !running; } 
	}
	
	public bool Running
    {
		get { return running; }
	}

    void Update()
    {	
        if (!active)
        {
            return;
        }
        
		if (running)
        {
			elapsedSeconds += Time.unscaledDeltaTime;
			if (elapsedSeconds >= totalSeconds)
            {
				running = false;
                timerFinishedEvent.Invoke();
			}
		}
	}
	
    public void Run()
    {	
        if (!active)
        {
            return;
        }
 
		if (totalSeconds > 0)
        {
			started = true;
			running = true;
            elapsedSeconds = 0;
		}
	}
}
